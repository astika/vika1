# Презентация книги Виктории Дмитриевой "Индия: бродячее блаженство" #

### Пройдёт в Лахте, в центре Бхакти-йоги по адресу ул. Морская д. 15 ###

![Презентация книги Виктории Дмитриевой "Индия: бродячее блаженство](http://i.imgur.com/0dWu5kn.jpg)

### О Вике, её книге и её презентациях: ###

* [https://www.youtube.com/watch?v=QSRgRzxtFT4&t=1562s](https://www.youtube.com/watch?v=QSRgRzxtFT4&t=1562s)
* [https://www.youtube.com/watch?v=hzZerr_sf1w](https://www.youtube.com/watch?v=hzZerr_sf1w)
* [https://www.youtube.com/watch?v=Og_IQcytgmU](https://www.youtube.com/watch?v=Og_IQcytgmU)

### Биография и описание книги ###

[Презентация книги](https://yadi.sk/i/aDLB1mcU3Yr9GG)

[Биография автора](https://yadi.sk/i/Ar1pLCYD3Yr9GM)
